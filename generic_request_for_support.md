# en - English

Hi,

You have reached the Debian press and publicity team. If you need
help to solve a problem, you can contact the Debian users mailing list:

debian-user@lists.debian.org
https://lists.debian.org/debian-user/

You can find more Debian support channels at:
https://www.debian.org/support


# es - Spanish

Hola,

Has escrito al equipo el equipo de prensa y publicidad de Debian.
Si necesitas ayuda en español para solucionar tu problema, puedes
contactar la lista de usuarios de Debian en español:

debian-user-spanish@lists.debian.org
https://lists.debian.org/debian-user-spanish/

Hay una lista de formas de obtener soporte en:
https://www.debian.org/support.es.html

Un saludo cordial,

# fr - French

Bonjour,

Vous avez contacté l'équipe de presse de Debian. Afin de trouver de
l'aide pour résoudre votre problème (en français), vous pouvez contacter
plutot la liste francophone des utilisateurs de Debian :

debian-user-french@lists.debian.org
https://lists.debian.org/debian-user-french/

Vous pouvez trouver une liste de modalités d'assistance sur :
https://www.debian.org/support.fr.html

Cordialement,


# it - Italian

Buongiorno,

Il vostro messaggio è arrivato al team delle pubbliche relazioni di Debian.
Se avete invece bisogno di aiuto per risolvere un problema, potete contattare
la mailing list in italiano degli utenti Debian:

debian-italian@lists.debian.org
https://lists.debian.org/debian-italian/

Ulteriori canali di supporto sono descritti su:
https://www.debian.org/support.it.html
