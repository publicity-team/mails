# en - English

Hi,

[OPTIONAL MAIL PERSONALISATION]

If you wish to stop receiving Debian announcements, please follow
these steps:

1. In your browser, open the following page:

    https://www.debian.org/MailingLists/unsubscribe

2.  Select there which mailing list you want to stop receiving. If you don't
remember which mailing lists you were subscribed to, check the email you
received and check what list it was addressed to.
For example, if it says debian-announce@lists.debian.org, click
debian-announce in the form on that page, 


3. Add your email address at the bottom of the page and click on unsubscribe.
It's very _important_ you use the email address you used to subscribe;
if you don't remember which email address you used, you can repeat
these steps with another email address.

4. Check your inbox - you'll receive an email asking you to confirm the
unsubscription, which you must reply to.


If you need more help, please check:
https://www.debian.org/MailingLists/#subunsub


# es - Spanish

Hola,

[PERSONALISATION DE EMAIL OPCIONAL ]

Si deseas dejar de recibir los emails de noticias de Debian, por favor
sigue estos pasos:

1. Abre tu navegador y carga esta página:

    https://www.debian.org/MailingLists/unsubscribe.es.html

2. Selecciona las listas de las cuales deseas darte de baja. Si no recuerdas
a que listas te subscribiste, mirar el último email que recibiste y
fijate que dirección de email sale en el campo de destinatario.
Por ejemplo, si pone  debian-announce@lists.debian.org debes seleccionar
debian-announce en el formulario de la página.

3. Pon tu dirección de correo abajo del todo de la página y clickea
en el botón 'Enviar desuscripción'. 
Es muy importante que uses la misma dirección de email que usaste
para subscribirte, si no recuerdas que dirección email usaste, puedes
repetir todo estos pasos con otra dirección de email.

4. Mirar tu buzón, recibirás un email pidiendote que confirmes la baja
que _debes_ responder para que esta sea efectiva.

Si necesitas, mas ayuda, por favor lee:
https://www.debian.org/MailingLists/index.es.html#subunsub



# fr - French

Bonjour,

[PERSONNALISATION OPTIONNELLE DU MESSAGE]

Si vous ne souhaitez plus recevoir les annonces de Debian, veuillez suivre
les étapes suivantes :

1. Dans votre navigateur ouvrez la page suivante :

    https://www.debian.org/MailingLists/unsubscribe

2.  Sélectionnez la liste de diffusion dont vous souhaitez vous désabonner.
Si vous avez oublié le nom des listes de diffusion auxquelles vous êtes
abonnés, vérifiez à quelle liste était adressé le courriel que vous avez reçu.
Par exemple, s'il s'agit de debian-announce@lists.debian.org, cochez la
case debian-announce dans le formulaire de cette page.

3. Ajoutez votre adresse électronique en bas de la page et cliquez sur « Se
désabonner ». Il est très _important_ que vous utilisiez l'adresse électronique
que vous avez utilisée pour vous abonner ; si vous ne savez plus quelle
adresse vous aviez indiquée, vous pouvez répéter ces étapes avec une autre
adresse électronique.

4. Vérifier votre courrier entrant : vous devriez recevoir un courriel vous
demandant de confirmer votre désabonnement auquel vous devez répondre.


Si vous avez besoin d'aide, veuillez consulter la page :
https://www.debian.org/MailingLists/#subunsub


# it - Italian

Buongiorno,

[PERSONALIZZAZIONE FACOLTATIVA DEL MESSAGGIO]

Se non si vogliono più ricevere gli annunci di Debian, seguire queste
istruzioni.

1.  Aprire con il proprio browser il seguente indirizzo:

    https://www.debian.org/MailingLists/unsubscribe


2.  Selezionare quali mailing list non si vogliono più ricevere. Se non si
ricorda a quali mailing list si era iscritti, controllare l'indirizzo di
destinazione della mail ricevuta.
Ad esempio, se dice debian-announce@lists.debian.org selezionare
debian-announce nel form di quella pagina.


3.  Aggiungere il proprio indirizzo email in fondo alla pagina e cliccare su
_unsubscribe_.
È molto _importante_ usare lo stesso indirizzo email con il quale ci si era
iscritti; se non lo si ricorda si possono ripetere questi passaggi con un altro
indirizzo email.

4.  Controllare la propria casella di posta: si riceverà una mail di conferma alla quale si deve rispondere per essere disiscritti.


Se si ha bisogno di ulteriore aiuto:
https://www.debian.org/MailingLists/#subunsub

